#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <signal.h>
#include <linux/spi/spidev.h>
#include <pthread.h>
#include <wsserver/ws.h>
#include <json-c/json.h>

#include "hsl.h"
#include "spi_lib.h"
#include "modellino.h"

int verbose_lev = 0;
#define dprintf(format, ...) if (verbose_lev) fprintf(stderr, format, ##__VA_ARGS__)

#define PIDfile "/run/modellino.pid"
//#define ConfFile "/run/modellino.pid"

// Framebuffer
#define animation_length 615
uint8_t PWManimation[animation_length][36]; // 615 frames

uint16_t wfm_anim[animation_length];
uint16_t wfm_anim_inv[animation_length];

uint32_t delay_us=16;

struct rgb inj = {.r = 206, .g = 71, .b = 131};  // Injection
double inj_l = 0.5;
struct rgb pr = {.r = 206, .g = 71, .b = 131};  // Power recycling
double pr_l = 0.5;
struct rgb bs = {.r = 206, .g = 71, .b = 131};  // Beam-splitter
double bs_l = 0.5;
struct rgb na = {.r = 206, .g = 71, .b = 131};  // North arm
double na_l = 0.5;
struct rgb wa = {.r = 206, .g = 71, .b = 131};  // West arm
double wa_l = 0.5;
struct rgb sr = {.r = 206, .g = 71, .b = 131};  // Signal recycling
double sr_l = 0.5;
struct rgb det = {.r = 206, .g = 71, .b = 131};  // Detection
double det_l = 0.5;
struct rgb tower = {.r = 206, .g = 71, .b = 131};  // Tower
double tower_l = 0.5;


spi_mode fd;
pthread_t thread_id; // SPI writer

FILE * fPtr = NULL;

//===== Modellino =====

// Colour, luminance, segment (0-7)
int colour(struct rgb col, double lum, int segment) {
  // wartosci H i S z danych RGB
  struct hsl desire_hsl;
  
  desire_hsl = struct_rgb_to_hsl(col.r/255., col.g/255., col.b/255.);
  
  double H = desire_hsl.h;
  double S = desire_hsl.s;
  
  //dprintf("desired H: %.4f, desired S: %.4f\n", H, S);
  
  // modulacja L danymi z GW150914
  struct rgb RGBarray;
  RGBarray = struct_hsl_to_rgb(H, S, lum);
  
  if (segment != NA){
    for (int i=0; i<animation_length; i++) {
      //dprintf("%f\n", wfm_anim[i]/4095.);
      //dprintf("R: %f\n", RGBarray.r);
      //dprintf("seg: %d\n",segment);
      //dprintf("val: %d\n",(uint16_t)(RGBarray.r*wfm_anim[i]));
      
      set_Raw(segment_to_channel(segment, RED), (uint16_t)(RGBarray.r*wfm_anim[i]), PWManimation[i]);
      set_Raw(segment_to_channel(segment, GR), (uint16_t)(RGBarray.g*wfm_anim[i]), PWManimation[i]);
      set_Raw(segment_to_channel(segment, BLU), (uint16_t)(RGBarray.b*wfm_anim[i]), PWManimation[i]);
    }
  } else {
    for (int i=0; i<animation_length; i++) {
      //dprintf("%f\n", wfm_anim[i]/4095.);
      //dprintf("R: %f\n", RGBarray.r);
      //dprintf("seg: %d\n",segment);
      //dprintf("val: %d\n",(uint16_t)(RGBarray.r*wfm_anim[i]));
      
      set_Raw(segment_to_channel(segment, RED), (uint16_t)(RGBarray.r*wfm_anim_inv[i]), PWManimation[i]);
      set_Raw(segment_to_channel(segment, GR), (uint16_t)(RGBarray.g*wfm_anim_inv[i]), PWManimation[i]);
      set_Raw(segment_to_channel(segment, BLU), (uint16_t)(RGBarray.b*wfm_anim_inv[i]), PWManimation[i]);
    }
  }
  
  return 0;
}


void parse_JSON_colour(struct json_object *section, struct rgb *col, double *lum) {
    struct json_object *colours;
    struct json_object *colour;
    struct json_object *luminance;
    
    if (json_object_object_get_ex(section, "rgb", &colours)) {
        json_object_object_get_ex(colours, "r", &colour);
        col->r = json_object_get_double(colour);
        json_object_object_get_ex(colours, "g", &colour);
        col->g = json_object_get_double(colour);
        json_object_object_get_ex(colours, "b", &colour);
        col->b = json_object_get_double(colour);
        
        dprintf("R: %.4f\nG: %.4f\nB: %.4f\n", col->r, col->g, col->b);
    }
    if (json_object_object_get_ex(section, "luminance", &luminance)) {
        *lum = json_object_get_double(luminance);
        
        dprintf("L: %.4f\n", *lum);
    }
}

void parse_JSON_msg(const unsigned char *msg) {
    struct json_object *parsed_json;
	struct json_object *delay;
	struct json_object *wfm;
	struct json_object *wfm_el;
	struct json_object *wfm_inv_el;
	json_bool tok_exists;
	size_t wfm_len;
    struct json_object *section;

	parsed_json = json_tokener_parse(msg);

	if (json_object_object_get_ex(parsed_json, "delay_us", &delay)) {
        delay_us = json_object_get_int(delay);
		//dprintf("Delay: %d\n", delay_us);
	}
	if (json_object_object_get_ex(parsed_json, "wfm", &wfm)) {
		wfm_len = json_object_array_length(wfm);
        if (wfm_len == animation_length) {
            for (int i=0;i<wfm_len;i++) {
                wfm_el = json_object_array_get_idx(wfm, i);
                wfm_anim[i] = json_object_get_int(wfm_el);
                //dprintf("%lu. %i\n", i+1, wfm_anim[i]);
            }
        } else {
            fprintf(stderr, "Wrong wfm array length\n", "string format", 30);
        }
	}
    if (json_object_object_get_ex(parsed_json, "wfm-inv", &wfm)) {
		wfm_len = json_object_array_length(wfm);
        if (wfm_len == animation_length) {
            //json_object_get_array(wfm)???
            for (int i=0;i<wfm_len;i++) {
                wfm_el = json_object_array_get_idx(wfm, i);
                wfm_anim_inv[i] = json_object_get_int(wfm_el);
                //dprintf("%lu. %i\n", i+1, wfm_anim_inv[i]);
            }
        } else {
            fprintf(stderr, "Wrong wfm-inv array length\n", "string format", 30);
        }
	}
    pthread_kill(thread_id, SIGUSR1);  // reset FB counter

   if (json_object_object_get_ex(parsed_json, "inj", &section)) {
       dprintf("inj:\n");
       parse_JSON_colour(section, &inj, &inj_l);
       colour(inj, inj_l, INJ);
   }
   if (json_object_object_get_ex(parsed_json, "pr", &section)) {
       dprintf("pr:\n");
       parse_JSON_colour(section, &pr, &pr_l);
       colour(pr, pr_l, PR);
   }
   if (json_object_object_get_ex(parsed_json, "bs", &section)) {
       dprintf("bs:\n");
       parse_JSON_colour(section, &bs, &bs_l);
       colour(bs, bs_l, BS);
   }
   if (json_object_object_get_ex(parsed_json, "na", &section)) {
       dprintf("na:\n");
       parse_JSON_colour(section, &na, &na_l);
       colour(na, na_l, NA);
   }
   if (json_object_object_get_ex(parsed_json, "wa", &section)) {
       dprintf("wa:\n");
       parse_JSON_colour(section, &wa, &wa_l);
       colour(wa, wa_l, WA);
   }
   if (json_object_object_get_ex(parsed_json, "sr", &section)) {
       dprintf("sr:\n");
       parse_JSON_colour(section, &sr, &sr_l);
       colour(sr, sr_l, SR);
   }
   if (json_object_object_get_ex(parsed_json, "det", &section)) {
       dprintf("det:\n");
       parse_JSON_colour(section, &det, &det_l);
       colour(det, det_l, DET);
   }

}


//===== WebSocket =====
void onopen(int fd) {
	char *cli;
	cli = ws_getaddress(fd);
	dprintf("Connection opened, client: %d | addr: %s\n", fd, cli);
	free(cli);
}


void onclose(int fd) {
	char *cli;
	cli = ws_getaddress(fd);
	dprintf("Connection closed, client: %d | addr: %s\n", fd, cli);
	free(cli);
}


void onmessage(int fd, const unsigned char *msg, uint64_t size, int type) {
	//dprintf("I receive a message: (%.*s) (size: %" PRId64 ", type: %d)\n", (int)size, msg, size, type);

    parse_JSON_msg(msg);

    ws_sendframe(fd, (char *)msg, size, true, type);
}

//===== Daemon =====
void clean_modellino() {
    if (remove(PIDfile)) {
		fprintf(stderr, "Unable to remove .pid file.\n", "string format", 30);
	}

	// Kill SPI writer
	pthread_kill(thread_id, 2);

	// Close SPI file
	close(fd.fd);
}

// Test with:
// $ kill -s signal-number pid
void signal_callback_handler(int signum) {
	dprintf("Caught signal %d\n",signum);
	// Cleanup and close up stuff here

	clean_modellino();

	// Terminate program
	exit(signum);
}

//===== SPI =====
int reset = 0;

void wrt_frame_signal_callback_handler(int signum) {
	reset = 1;
    dprintf("SIGUSR1 caught by spiWriteThread!\n");
}

static void wrt_frame() {
	int ret;
    struct spi_ioc_transfer tr = {
        //.tx_buf = (unsigned long)NULL,
        //.rx_buf = (unsigned long)NULL,
        .len = 36,
        .delay_usecs = delay_us,
        .speed_hz = 500000,
        .bits_per_word = 8,
    };
	for (int i=0; i<animation_length; i++) { // This does not generate pauses!!
        tr.tx_buf = (unsigned long)PWManimation[i];
		ret = ioctl(fd.fd, SPI_IOC_MESSAGE(1), &tr);

        if (reset) {
            i=0; // reset loop iterator
            
            tr.delay_usecs = delay_us;
            
            reset = 0;
            dprintf("Frame buffer counter reset.\n");
        }
		//sleep(1);
	}
}


void *spiWriteThread(void *vargp) {
    // Register signal and signal handler
	if (signal(SIGUSR1, wrt_frame_signal_callback_handler) == SIG_ERR) {
        fprintf(stderr, "Unable to register SIGUSR1 handler.\n", "string format", 36);
		exit(EXIT_FAILURE);
    }
    
	for (;1;) { // seems a little faster between operations
		wrt_frame();
	}

	return NULL; 
}


int main(int argc, char *argv[]) {
    // Parse command line args
    int opt;
    
    while ((opt = getopt(argc, argv, "v")) != -1) {
        switch (opt) {
        case 'v': verbose_lev = 1; break;
        default:
            fprintf(stderr, "Usage: %s [-v]\n", argv[0]);
            exit(EXIT_FAILURE);
        }
    }
    
    dprintf("Debug mode is on!\n");
    
	// Register signal and signal handler
	if (signal(SIGINT, signal_callback_handler) == SIG_ERR) {
        fprintf(stderr, "Unable to register SIGINT handler.\n", "string format", 36);
		exit(EXIT_FAILURE);
    }
    if (signal(SIGTERM, signal_callback_handler) == SIG_ERR) {
        fprintf(stderr, "Unable to register SIGTERM handler.\n", "string format", 37);
		exit(EXIT_FAILURE);
    }
    if (signal(SIGABRT, signal_callback_handler) == SIG_ERR) {
        fprintf(stderr, "Unable to register SIGABRT handler.\n", "string format", 37);
		exit(EXIT_FAILURE);
    }

	pid_t pid;

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0) {
		fprintf(stderr, "Unable to fork.\n", "string format", 30);
		exit(EXIT_FAILURE);
	}
	/* If we got a good PID, then
		 we can exit the parent process. */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);

	/* Open any logs here */

	/* Create a new SID for the child process */
	pid_t sid = setsid();
	if (sid < 0) {
		/* Log any failure */
		fprintf(stderr, "Unable to create SID.\n", "string format", 30);
		exit(EXIT_FAILURE);
	}

	/* Change the current working directory */
	if ((chdir("/")) < 0) {
		/* Log any failure here */
		fprintf(stderr, "Unable to change working pwd.\n", "string format", 30);
		exit(EXIT_FAILURE);
	}

	/* Close out the standard file descriptors */
	//close(STDIN_FILENO);
	//close(STDOUT_FILENO);
	//close(STDERR_FILENO);

	/* Daemon-specific initialization goes here */
    if (access(PIDfile, F_OK) == 0) {
        FILE* fp = fopen(PIDfile, "r");
        char buffer[10];
        fread(buffer, sizeof(char), sizeof(buffer), fp);
        fclose(fp);
        
        pid_t old_pid = atoi(buffer);
        if (kill(old_pid, 0) == 0) {
            fprintf(stderr, "Daemon is already running. PID: %d\n", old_pid, "string format", 30);
            exit(EXIT_FAILURE);
        }
        else {
            remove(PIDfile);
        }
    }
    
	pid_t pidn = getpid();
	dprintf("%d\n",pidn); // write to some file where one can read it

	fPtr = fopen(PIDfile, "write-mode");
	if (fPtr == NULL) {
		fprintf(stderr, "Unable to open .pid file.\n", "string format", 30);
		exit(EXIT_FAILURE);
	}

	fprintf(fPtr, "%d\n", pidn);
	fclose(fPtr);

	/* Daemon-specific body */
	/* Init SPI and create writing thread */
    SPI_init(&fd, 500000);
	pthread_create(&thread_id, NULL, spiWriteThread, NULL); 

	/* Start web socket */
	struct ws_events evs;
	evs.onopen    = &onopen;
	evs.onclose   = &onclose;
	evs.onmessage = &onmessage;
	ws_socket(&evs, 8080); // Should never end

    clean_modellino();
	exit(EXIT_FAILURE);
}
