#include "hsl.h"
#include "modellino.h"

#include "GW150914_wfm.h" // 615 values
#include "GW150914_wfm_inv.h"

// Framebuffer
#define animation_length 615
uint8_t PWManimation[animation_length][36]; // 615 frames

// All operation on colours is done on doubles [0-1]

// colour I want:
struct rgb desire = {.r = 206, .g = 71, .b = 131};

uint8_t segment = 0;

float gain = 1.; // 100% full luminance -> 1.

struct rgb RGBarray[animation_length];

int colour(uint8_t * tab) {
  // wartosci H i S z danych RGB
  struct hsl desire_hsl;
  
  desire_hsl = struct_rgb_to_hsl(desire.r/255, desire.g/255, desire.b/255);
  
  double H = desire_hsl.h;
  double S = desire_hsl.s;
  
  printf("desired H: %.4f, desired S: %.4f\n", H, S);
  
  // modulacja L danymi z GW150914
  for(int i=0; i<animation_length; i++) {
    RGBarray[i] = struct_hsl_to_rgb(H, S, gw150914[i]/4095.);
    //printf("%f\n", gw150914[i]/4095.);
    //printf("%f\n", RGBarray[i].b);
    
    set_Raw(segment_to_channel(segment, RED), (uint16_t)gain*RGBarray[i].r*4095., PWManimation[i]);
    set_Raw(segment_to_channel(segment, GR), (uint16_t)gain*RGBarray[i].g*4095., PWManimation[i]);
    set_Raw(segment_to_channel(segment, BLU), (uint16_t)gain*RGBarray[i].b*4095., PWManimation[i]);
  }
  //memccpy(tab, &PWManimation, 615*36*sizeof(uint8_t));
  
  return 0;
}

int main(int argc, char *argv[])
{
  printf("desired R: %d, desired G: %d\n", desire.r, desire.g);

  // wartosci H i S z danych RGB
  struct hsl desire_hsl;
  
  desire_hsl = struct_rgb_to_hsl(desire.r/255, desire.g/255, desire.b/255);
  
  double H = desire_hsl.h;
  double S = desire_hsl.s;
  
  printf("desired H: %.4f, desired S: %.4f\n", H, S);
  
  // modulacja L danymi z GW150914
  for(int i=0; i<animation_length; i++) {
    RGBarray[i] = struct_hsl_to_rgb(H, S, gw150914[i]/4095.);
    //printf("%f\n", gw150914[i]/4095.);
    //printf("%f\n", RGBarray[i].b);
    
    set_Raw(segment_to_channel(segment, RED), (uint16_t)gain*RGBarray[i].r*4095., PWManimation[i]);
    set_Raw(segment_to_channel(segment, GR), (uint16_t)gain*RGBarray[i].g*4095., PWManimation[i]);
    set_Raw(segment_to_channel(segment, BLU), (uint16_t)gain*RGBarray[i].b*4095., PWManimation[i]);
  }
  
  
  return 0;
}
