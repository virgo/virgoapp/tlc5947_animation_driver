// SPDX-License-Identifier: GPL-2.0-only
/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <fcntl.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#ifndef SPI_TEST_H
#define SPI_TEST_H

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))


typedef struct {
  int fd;
  uint32_t mode;
  uint8_t bits;
  char *output_file;
  uint32_t speed;
  uint16_t delay;
  int verbose;
  int interval; /* interval in seconds for showing transfer rate */
  struct spi_ioc_transfer tr;
}spi_mode;


void pabort(const char *s);

void hex_dump(const void *src, size_t length, size_t line_size,
		     char *prefix);

/*
 *  Unescape - process hexadecimal escape character
 *      converts shell input "\x23" -> 0x23
 */
int unescape(char *_dst, char *_src, size_t len);

int transfer(spi_mode fd, uint8_t const *tx, uint8_t const *rx, size_t len);

void transfer_escaped_string(spi_mode fd, char *str);

void transfer_file(spi_mode fd, char *filename);

uint64_t _read_count;
uint64_t _write_count;

void show_transfer_rate(spi_mode fd);

void transfer_buf(spi_mode fd, int len);

int SPI_init(spi_mode *fd, int speed);

//int main(int argc, char *argv[]);

#endif //SPI_TEST_H
