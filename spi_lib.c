// SPDX-License-Identifier: GPL-2.0-only
/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <fcntl.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include "spi_lib.h"

//uint8_t bits = 8;
//uint32_t speed = 500000;
//int interval = 5; /* interval in seconds for showing transfer rate */

void pabort(const char *s)
{
	if (errno != 0)
		perror(s);
	else
		printf("%s\n", s);

	abort();
}

//const char *device = "/dev/spidev0.1";

//
//uint8_t default_tx[] = {
//	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
//	0x40, 0x00, 0x00, 0x00, 0x00, 0x95,
//	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
//	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
//	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
//	0xF0, 0x0D,
//};
//
//uint8_t default_rx[ARRAY_SIZE(default_tx)] = {0, };
//char *input_tx;

void hex_dump(const void *src, size_t length, size_t line_size,
		     char *prefix)
{
	int i = 0;
	const unsigned char *address = src;
	const unsigned char *line = address;
	unsigned char c;

	printf("%s | ", prefix);
	while (length-- > 0) {
		printf("%02X ", *address++);
		if (!(++i % line_size) || (length == 0 && i % line_size)) {
			if (length == 0) {
				while (i++ % line_size)
					printf("__ ");
			}
			printf(" |");
			while (line < address) {
				c = *line++;
				printf("%c", (c < 32 || c > 126) ? '.' : c);
			}
			printf("|\n");
			if (length > 0)
				printf("%s | ", prefix);
		}
	}
}

/*
 *  Unescape - process hexadecimal escape character
 *      converts shell input "\x23" -> 0x23
 */
int unescape(char *_dst, char *_src, size_t len)
{
	int ret = 0;
	int match;
	char *src = _src;
	char *dst = _dst;
	unsigned int ch;

	while (*src) {
		if (*src == '\\' && *(src+1) == 'x') {
			match = sscanf(src + 2, "%2x", &ch);
			if (!match)
				pabort("malformed input string");

			src += 4;
			*dst++ = (unsigned char)ch;
		} else {
			*dst++ = *src++;
		}
		ret++;
	}
	return ret;
}

int transfer(spi_mode fd, uint8_t const *tx, uint8_t const *rx, size_t len)
{
	int ret;

//	if (fd.mode & SPI_TX_QUAD)
//		tr.tx_nbits = 4;
//	else if (fd.mode & SPI_TX_DUAL)
//		tr.tx_nbits = 2;
//	if (fd.mode & SPI_RX_QUAD)
//		tr.rx_nbits = 4;
//	else if (fd.mode & SPI_RX_DUAL)
//		tr.rx_nbits = 2;
//	if (!(fd.mode & SPI_LOOP)) {
//		if (fd.mode & (SPI_TX_QUAD | SPI_TX_DUAL))
//			tr.rx_buf = 0;
//		else if (fd.mode & (SPI_RX_QUAD | SPI_RX_DUAL))
//			tr.tx_buf = 0;
//	}

	//ret = ioctl(fd.fd, SPI_IOC_MESSAGE(1), &tr);
	return ret;
}

void transfer_escaped_string(spi_mode fd, char *str)
{
	size_t size = strlen(str);
	uint8_t *tx;
	uint8_t *rx;

	tx = malloc(size);
	if (!tx)
		pabort("can't allocate tx buffer");

	rx = malloc(size);
	if (!rx)
		pabort("can't allocate rx buffer");

	size = unescape((char *)tx, str, size);
	transfer(fd, tx, rx, size);
	free(rx);
	free(tx);
}

void transfer_file(spi_mode fd, char *filename)
{
	ssize_t bytes;
	struct stat sb;
	int tx_fd;
	uint8_t *tx;
	uint8_t *rx;

	if (stat(filename, &sb) == -1)
		pabort("can't stat input file");

	tx_fd = open(filename, O_RDONLY);
	if (tx_fd < 0)
		pabort("can't open input file");

	tx = malloc(sb.st_size);
	if (!tx)
		pabort("can't allocate tx buffer");

	rx = malloc(sb.st_size);
	if (!rx)
		pabort("can't allocate rx buffer");

	bytes = read(tx_fd, tx, sb.st_size);
	if (bytes != sb.st_size)
		pabort("failed to read input file");

	transfer(fd, tx, rx, sb.st_size);
	free(rx);
	free(tx);
	close(tx_fd);
}

void show_transfer_rate(spi_mode fd)
{
	uint64_t prev_read_count, prev_write_count;
	double rx_rate, tx_rate;

	rx_rate = ((_read_count - prev_read_count) * 8) / (fd.interval*1000.0);
	tx_rate = ((_write_count - prev_write_count) * 8) / (fd.interval*1000.0);

	printf("rate: tx %.1fkbps, rx %.1fkbps\n", rx_rate, tx_rate);

	prev_read_count = _read_count;
	prev_write_count = _write_count;
}

void transfer_buf(spi_mode fd, int len)
{
	uint8_t *tx;
	uint8_t *rx;
	int i;

	tx = malloc(len);
	if (!tx)
		pabort("can't allocate tx buffer");
	for (i = 0; i < len; i++)
		tx[i] = random();

	rx = malloc(len);
	if (!rx)
		pabort("can't allocate rx buffer");

	transfer(fd, tx, rx, len);

	_write_count += len;
	_read_count += len;

	if (fd.mode & SPI_LOOP) {
		if (memcmp(tx, rx, len)) {
			fprintf(stderr, "transfer error !\n");
			hex_dump(tx, len, 32, "TX");
			hex_dump(rx, len, 32, "RX");
			exit(1);
		}
	}

	free(rx);
	free(tx);
}

int SPI_init(spi_mode *fd, int speed) {
	static const char *device = "/dev/spidev0.1";

	int ret = 0;
	fd->speed = speed;
	//delay = atoi("8"); //us
	fd->bits = 8;

	//mode |= SPI_CPHA;
	//mode |= SPI_CPOL;
	//mode |= SPI_LSB_FIRST;
	//mode |= SPI_CS_HIGH;
	//mode |= SPI_NO_CS;
	//mode |= SPI_READY;
	//mode |= SPI_TX_DUAL;
	//mode |= SPI_TX_QUAD;
	//transfer_size = atoi("36");
	//iterations = atoi(optarg);

	if (fd->mode & SPI_LOOP) {
		if (fd->mode & SPI_TX_DUAL)
			fd->mode |= SPI_RX_DUAL;
		if (fd->mode & SPI_TX_QUAD)
			fd->mode |= SPI_RX_QUAD;
    }
    
	fd->fd = open(device, O_RDWR);
	if (fd->fd < 0)
		pabort("can't open device");

	/*
	 * spi mode
	 */
	ret = ioctl(fd->fd, SPI_IOC_WR_MODE32, &fd->mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(fd->fd, SPI_IOC_RD_MODE32, &fd->mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd->fd, SPI_IOC_WR_BITS_PER_WORD, &fd->bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(fd->fd, SPI_IOC_RD_BITS_PER_WORD, &fd->bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd->fd, SPI_IOC_WR_MAX_SPEED_HZ, &fd->speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(fd->fd, SPI_IOC_RD_MAX_SPEED_HZ, &fd->speed);
	if (ret == -1)
		pabort("can't get max speed hz");

	printf("spi mode: 0x%x\n", fd->mode);
	printf("bits per word: %u\n", fd->bits);
	printf("max speed: %u Hz (%u kHz)\n", fd->speed, fd->speed/1000);

	return ret;
}
