colours: colours.c
	gcc -o colours.out colours.c modellino.c -std=gnu11 -lm 

virghost: virghost.c spi_lib.c
	gcc -o virghost.out virghost.c spi_lib.c modellino.c -lpthread -lws -ljson-c -lm

clean:
	$(RM) colours.out
	$(RM) virghost.out
