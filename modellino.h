#include <stdint.h>

#ifndef MODELLINO_H
#define MODELLINO_H

#define ANIMATION_LENGTH 615

// PWM driver has 24 channels 0-23. Channel 0-2 drives three colours of one tube segment.
//      colour  offset
#define RED     0
#define GR      1
#define BLU     2

// TODO: check real connections!
//      name    PWN segment
#define NA      6  // 6
#define WA      4  // 4
#define BS      2  // 2
#define PR      1  // 1
#define SR      0  // 0
#define INJ     7  // 7
#define DET     5  // 5
#define TOWER   3 // 3 - does not exist since RGB LED in the tower needs another driver

//Data type we are operating on (24 channels x 12bit)
//uint8_t default[36] = {0, ...};

// Extracts from uint8_t array [table] 12 bit PWM value in the chennel [ch].
uint16_t get_Raw(uint8_t ch, uint8_t* table);

// LED channel, 16bit value, 36Bajt output vector
void set_Raw(uint8_t ch, uint16_t val, uint8_t table[]);

uint8_t segment_to_channel(int segment, int colour);

#endif //MODELLINO_H
