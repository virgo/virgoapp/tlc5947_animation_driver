#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <signal.h>


#define PIDfile "/run/modellino.pid"

FILE * fPtr = NULL;

void signal_callback_handler(int signum) {
  printf("Caught signal %d\n",signum);
  // Cleanup and close up stuff here

  if (!remove(PIDfile)) {
    fprintf(stderr, "Unable to remove .pid file.\n", "string format", 30);
  }

  // Terminate program
  exit(signum);
}


int main(int argc, char *argv[])
{
  // Register signal and signal handler
  signal(SIGINT, signal_callback_handler);

  pid_t pid;

  /* Fork off the parent process */       
  pid = fork();
  if (pid < 0) {
    fprintf(stderr, "Unable to fork.\n", "string format", 30);
    exit(EXIT_FAILURE);
  }
  /* If we got a good PID, then
     we can exit the parent process. */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  /* Change the file mode mask */
  umask(0);

  /* Open any logs here */
        
  /* Create a new SID for the child process */
  pid_t sid = setsid();
  if (sid < 0) {
    /* Log any failure */
    fprintf(stderr, "Unable to create sid.\n", "string format", 30);
    exit(EXIT_FAILURE);
  }

  /* Change the current working directory */
  if ((chdir("/")) < 0) {
    /* Log any failure here */
    fprintf(stderr, "Unable to change working pwd.\n", "string format", 30);
    exit(EXIT_FAILURE);
  }

  /* Close out the standard file descriptors */
  //close(STDIN_FILENO);
  //close(STDOUT_FILENO);
  //close(STDERR_FILENO);

  /* Daemon-specific initialization goes here */
  pid_t pidn = getpid();
  printf("%d\n",pidn); // write to some file where one can read it

  fPtr = fopen(PIDfile, "write-mode");
  if (fPtr == NULL) {
    fprintf(stderr, "Unable to open .pid file.\n", "string format", 30);
    exit(EXIT_FAILURE);
  }

  fprintf(fPtr, "%d\n", pidn);
  fclose(fPtr);


  //

  char temp[100];
  while(1) {
    scanf("%s", temp);
    printf("%s", temp);
    sleep(1);
  }
  exit(EXIT_SUCCESS);
}
