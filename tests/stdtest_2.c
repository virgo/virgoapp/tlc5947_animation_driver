#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
  int pid = getpid();
  printf("%d\n",pid);

  char temp[100];
  while(1) {
    scanf("%s", temp);
    printf("%s", temp);
  }
  return 0;
}
