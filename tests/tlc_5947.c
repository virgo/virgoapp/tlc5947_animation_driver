#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <fcntl.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <pthread.h>
#include <signal.h>


#include "spi_test.h"
#include "modellino.h"

#include "GW150914_wfm.h" // 615 values
#include "GW150914_wfm_inv.h"

//TODO Check real duration of the pattern and write it in the .h file!

// Framebuffer
#define animation_length 615
uint8_t PWManimation[615][36]; // 615 frames
static uint8_t default_rx[36] = {0, };

spi_mode fd;
pthread_t thread_id;


// LED channel, animation waveform, output array
void wrt_seq_to_frame(uint8_t ch, uint16_t anim[], uint8_t frame[][36]) {
  
  for (int i=0; i<animation_length; i++) {
    set_Raw(ch, anim[i], frame[i]);
    //vprintf("step: %d\n", i);
  }
}

static void wrt_frame() {
  int ret;
  for (int i=0; i<animation_length; i++) { // This does not generate pauses!!
    struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)PWManimation[i],
		.rx_buf = (unsigned long)NULL,
		.len = 36,
		.delay_usecs = 16,
		.speed_hz = 500000,
		.bits_per_word = 8,
    };
    ret = ioctl(fd.fd, SPI_IOC_MESSAGE(1), &tr);
    //sleep(1);
  }
}

static const char *device = "/dev/spidev0.1";
//static uint32_t mode;
static char *input_file;
static int transfer_size;
static int iterations;


void SPI_init() {
  fd.speed = atoi("500000");
  //delay = atoi("8"); //us
  fd.bits = 8;

  //mode |= SPI_CPHA;
  //mode |= SPI_CPOL;
  //mode |= SPI_LSB_FIRST;
  //mode |= SPI_CS_HIGH;
  //mode |= SPI_NO_CS;
  //mode |= SPI_READY;
  //mode |= SPI_TX_DUAL;
  //mode |= SPI_TX_QUAD;
  //transfer_size = atoi("36");
  //iterations = atoi(optarg);
  
  if (fd.mode & SPI_LOOP) {
    if (fd.mode & SPI_TX_DUAL)
      fd.mode |= SPI_RX_DUAL;
    if (fd.mode & SPI_TX_QUAD)
      fd.mode |= SPI_RX_QUAD;
  }
}

int init() {
  int ret = 0;

  SPI_init();

  fd.fd = open(device, O_RDWR);
  if (fd.fd < 0)
    pabort("can't open device");

  /*
   * spi mode
   */
  ret = ioctl(fd.fd, SPI_IOC_WR_MODE32, &fd.mode);
  if (ret == -1)
    pabort("can't set spi mode");

  ret = ioctl(fd.fd, SPI_IOC_RD_MODE32, &fd.mode);
  if (ret == -1)
    pabort("can't get spi mode");

  /*
   * bits per word
   */
  ret = ioctl(fd.fd, SPI_IOC_WR_BITS_PER_WORD, &fd.bits);
  if (ret == -1)
    pabort("can't set bits per word");

  ret = ioctl(fd.fd, SPI_IOC_RD_BITS_PER_WORD, &fd.bits);
  if (ret == -1)
    pabort("can't get bits per word");

  /*
   * max speed hz
   */
  ret = ioctl(fd.fd, SPI_IOC_WR_MAX_SPEED_HZ, &fd.speed);
  if (ret == -1)
    pabort("can't set max speed hz");

  ret = ioctl(fd.fd, SPI_IOC_RD_MAX_SPEED_HZ, &fd.speed);
  if (ret == -1)
    pabort("can't get max speed hz");

  printf("spi mode: 0x%x\n", fd.mode);
  printf("bits per word: %u\n", fd.bits);
  printf("max speed: %u Hz (%u kHz)\n", fd.speed, fd.speed/1000);

  return ret;
}

void *spiWriteThread(void *vargp) 
{
  for (;1;) { // seems a little faster between operations
    wrt_frame();
  }
  
  return NULL; 
}

int main(int argc, char *argv[])
{
  SPI_init();

  init();
  
  uint8_t NA_R_ch = segment_to_channel(arm_N, GR);
  uint8_t WA_R_ch = segment_to_channel(arm_W, GR);

  wrt_seq_to_frame(NA_R_ch, gw150914, PWManimation);
  wrt_seq_to_frame(WA_R_ch, gw150914, PWManimation);

  printf("Sequence writen");
  
  for (;1;) { // seems a little faster between operations
    wrt_frame();
    return(0);
  }
  
//  printf("After for ;1;\n");
//  do { // check also this!!
//    wrt_frame();
//  } while(1);
    
//  printf("After do while\n");
//  while (1) { // while loop creates pause after each iteration. Why???
//    wrt_frame();
//    //printf("d");
//  }
  
  pthread_create(&thread_id, NULL, spiWriteThread, NULL); 
  //pthread_join(thread_id, NULL); // wait to end
  
  sleep(5);
  pthread_kill(thread_id, 2); //SIGINT
  

  close(fd.fd);

  return 0;
}

