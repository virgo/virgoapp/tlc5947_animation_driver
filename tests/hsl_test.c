#include "../hsl.h"



int main(int argc, char *argv[])
{
  double h,s,l,r,g,b;
  double *array;
  double *arr;
  
  int i = 255;
  int j = 0;
  int k = 0;
  
  array = rgb_to_hsl(i/255.0, j/255.0, k/255.0);
   h = array[0];
   s = array[1];
   l = array[2];
   free(array);
   
   arr = hsl_to_rgb(h, s, l);
   r = round(arr[0] * 255.0);
   g = round(arr[1] * 255.0);
   b = round(arr[2] * 255.0);
  free(arr);
  
  printf("\nRGB VALUES:R:%i G:%i B:%i ", i, j, k);
  printf("\nRGB VALUES:R:%f G:%f B:%f ", r, g, b);
  printf("\nhue: %f, lum: %f, sat: %f \n", h, l, s);

  return 0;
}
