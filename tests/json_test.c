#include <stdio.h>
#include <unistd.h>
#include <json-c/json.h>

#define BUFFER_LENGTH 4096

int main(int argc, char **argv) {
	FILE *fp;
	char buffer[BUFFER_LENGTH];
	struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *delay;
	struct json_object *friends;
	struct json_object *friend;
	size_t n_friends;

	size_t i;

	fp = fopen("test.json","r");
	fread(buffer, BUFFER_LENGTH, 1, fp);
	fclose(fp);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "delay", &delay);
	json_object_object_get_ex(parsed_json, "waveform", &friends);

	printf("Delay: %d\n", json_object_get_int(delay));

	n_friends = json_object_array_length(friends);
	//printf("Found %lu friends\n",n_friends);

	for(i=0;i<n_friends;i++) {
		friend = json_object_array_get_idx(friends, i);
		printf("%lu. %i\n",i+1,json_object_get_int(friend));
	}

	json_object_object_get_ex(parsed_json, "inj", &delay);
	json_object_object_get_ex(delay, "rgb", &friends);
	json_object_object_get_ex(friends, "r", &friend);
	printf("R: %d\n", json_object_get_int(friend));
}
