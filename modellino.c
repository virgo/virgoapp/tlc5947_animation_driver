#include "modellino.h"

uint16_t get_Raw(uint8_t ch, uint8_t* table) {
  uint16_t val;

  uint8_t base_addr = 23 - ch;
  base_addr += base_addr >> 1;
  
  if (ch % 2) { // reszta 1 -> kanal nieparzysty
    //val = *(uint16_t*)(&(table[base_addr]));
    ((uint8_t*)&val)[1] = table[base_addr];
    ((uint8_t*)&val)[0] = table[base_addr+1];

    val = val >> 4;
  }
  else { // kanal parzysty
    //val = *(uint16_t*)(&(table[base_addr]));
    ((uint8_t*)&val)[1] = table[base_addr];
    ((uint8_t*)&val)[0] = table[base_addr+1];
    //val = (uint16_t*)table[base_addr]/;
    val = val & 0x0FFF;
  }
  
  return val;
}

// LED channel, 16bit value (only 12 bit are used), 36 Byte output vector
void set_Raw(uint8_t ch, uint16_t val, uint8_t table[]) {
  uint8_t base_addr = 23 - ch;
  base_addr += base_addr >> 1;
  
  if (ch % 2) { // reszta 1 -> kanal nieparzysty
    val = val << 4;
    table[base_addr] = ((uint8_t*)&val)[1];
    table[base_addr+1] = (((uint8_t*)&val)[0] & 0xF0) | (table[base_addr+1] & 0x0F);
  }
  else { // kanal parzysty
    table[base_addr] = (((uint8_t*)&val)[1] & 0x0F) | (table[base_addr] & 0xF0);
    table[base_addr+1] = ((uint8_t*)&val)[0];
  }
}

uint8_t segment_to_channel(int segment, int colour) {
  return segment*3 + colour;
}
